// ? task - 1:
// TODO: დაწერეთ ფუნქცია, რომელიც გამოიტანს სტრიქონის სიგრძეს.

function lengthOfString(text) {
  console.log(text.length);
}

lengthOfString("Misho magaria");

// ? task - 2
// Todo: დაწერეთ ფუნქცია, რომელიც დაითვლის სტრიქონში სიმბოლო a-ის რაოდენობას.

function howManyA(text) {
  let string = text.toLowerCase();
  let letter = 0;
  for (i of string) {
    if (i == "a") {
      letter += 1;
    }
  }

  console.log(`A = ${letter}`);
}

howManyA("MishoaaaA");

// ? task - 3 :
// todo: დაწერეთ ფუნქცია, რომელიც სტრიქონში იპოვის ქვესტრიქონის რაოდენობას.

function spliter(text) {
  let splited = text.split(" ");
  console.log(splited.length);
}

spliter("Misho magari kacia");

// ? task - 4 :
// todo დაწერეთ ფუნქცია, რომელიც გამოიტანს სტრიქონის შიდა კოდებს.

function getInternalCodes(text) {
  let codes = [];
  for (let i = 0; i < text.length; i++) {
    codes.push(text.charCodeAt(i));
  }
  console.log(codes);
}

getInternalCodes("Hello world my name is js");
// ? task - 5 :
// todo: დაწერეთ ფუნქცია, რომელიც დააგენერირებს 40 შემთხვევითი სიმბოლოსაგან შემდგარ სიტყვას.

function randomWord() {
  let symbols = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    "(",
    ")",
    "_",
    "+",
    "-",
    "=",
    "[",
    "]",
    "{",
    "}",
    "|",
    ";",
    ":",
    ",",
    ".",
    "<",
    ">",
    "?",
  ];
  let result = "";

  for (let i = 0; i < 40; i++) {
    let randomIndex = Math.floor(Math.random() * symbols.length);
    result += symbols[randomIndex];
  }

  console.log(result);
}

randomWord();

// ? task - 6 :
// todo დაწერეთ ფუნქცია, რომელიც დააგენერირებს n შემთხევითი სიმბოლოსგან შემდგარ სიტყვას.

function randomWordAnyLength(lengthOfSymbols) {
  let symbols = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    "(",
    ")",
    "_",
    "+",
    "-",
    "=",
    "[",
    "]",
    "{",
    "}",
    "|",
    ";",
    ":",
    ",",
    ".",
    "<",
    ">",
    "?",
  ];
  let result = "";

  for (let i = 0; i < lengthOfSymbols; i++) {
    let randomIndex = Math.floor(Math.random() * symbols.length);
    result += symbols[randomIndex];
  }

  console.log(result);
}

randomWordAnyLength(100);
// ? task - 7 :
// todo დაწერეთ ფუნქცია, რომელიც დააგენერირებს n შემთხევითი სიმბოლოსგან შემდგარი სიტყვებით შედგენილ 20 სიტყვიან სტრიქონს გამოტოვებებით.

function randomWordWithSpaces(lengthOfSymbols) {
  let symbols = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "!",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    "(",
    ")",
    "_",
    "+",
    "-",
    "=",
    "[",
    "]",
    "{",
    "}",
    "|",
    ";",
    ":",
    ",",
    ".",
    "<",
    ">",
    "?",
  ];
  let result = "";
  let num = Math.floor(lengthOfSymbols / 20);
  let n = 1;

  for (let i = 0; i < lengthOfSymbols; i++) {
    if (num > 1 && i > 1 && i % num === 0 && n < 20 && lengthOfSymbols > 20) {
      result += " ";
      n++;
    } else {
      let randomIndex = Math.floor(Math.random() * symbols.length);
      result += symbols[randomIndex];
    }
  }

  console.log(result);
  console.log(result.split(" ").length);
}
randomWordWithSpaces(40);

// ? task - 8 :
// todo დაწერეთ ფუნქცია, რომელიც დააგენერირებს მინიმალური m და
// todo მაქსიმალური n შემთხვევითი სიმბოლოსგან შემდგარი სიტყვების k
// todo რაოდენობისაგან შემდგარ სტრიქონს.

function randomWithmnk(m, n, k) {}

// ? task - 9 :
// todo: დაწერეთ ფუნქცია რომელიც დაადგენს სტრიქონში მოიძებნება თუ არა სტრიქონების მასივში ჩაწერილი სიტყვები.

// ? task - 10 :
// todo:
