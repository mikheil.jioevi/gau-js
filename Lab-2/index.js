// Task 1:
//TODO: 1. დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამრგვალებს მეტობით.

function roundByCeil(x) {
  console.log(Math.ceil(x));
}

roundByCeil(12.3);

// task 2:
//TODO: 2. დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამრგვალებს ნაკლებობით.

function roundByFloor(x) {
  console.log(Math.floor(x));
}

roundByFloor(12.6);

// task 3:
//TODO: 3. დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამრგვალებს.

function round(x) {
  console.log(Math.round(x));
}

round(14.3);

// task 4:
//TODO: 4. დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამრგვალებს, დაამრგვალებს მეტობით ან ნაკლებობით, მეორე
// გადაცემული პა-რამეტრის მიხედვით (მეორე პარამეტრი უნდა იყოს ლოგიკური).

function roundByCondition(x, condition) {
  switch (condition) {
    case 1:
      console.log(Math.ceil(x) + " Damrgvaleba metobit");
      break;

    case 2:
      console.log(Math.floor(x) + " Damrgvaleba naklebobit");
      break;

    case 3:
      console.log(Math.round(x) + " Chveulebrivi damrgvaleba");
      break;

    default:
      console.log(x + " Gtxovt Sheiyvanot ricxvi 1-dan 3-mde");
  }
}

roundByCondition(4.34, 1);

// task 5:
//TODO: 5. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს შემთხვევით რიცხვს [0, 1] შუალედიდან.

function randomZeroOne() {
  console.log(Math.random());
}

randomZeroOne();

// task 6:
//TODO: 6. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს შემთხვევით რიცხვს [5, 50] შუალედიდან.

function randomFiveFifty() {
  console.log(Math.random() * (50 - 5) + 5);
}

randomFiveFifty();

// task 7:
//TODO: 7. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს შემთხვევით რიცხვს პარამეტრებად გადაცემული [a, b] შუალედიდან.

function randomFromTo(a, b) {
  if (a > b) {
    console.log(Math.random() * (a - b) + b);
  } else {
    console.log(Math.random() * (b - a) + a);
  }
}

randomFromTo(100, 5);

// task 8:
//TODO: 8. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს შემთხვევით მთელ რიცხვს პარამეტრებად გადაცემული [a, b] მთელი შუალედიდან.

function randomFromToRound(a, b) {
  if (a > b) {
    console.log(Math.round(Math.random() * (a - b) + b));
  } else {
    console.log(Math.round(Math.random() * (b - a) + a));
  }
}

randomFromToRound(50, 100);

// task 9:
//TODO: 9. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს 10 შემთხვევით მთელ რიცხვს პარამეტრებად გადაცემულ [a, b] მთელი შუალედიდან.

function tenRandom(a, b) {
  console.log("Mecxre!");
  for (i = 1; i <= 10; i++) {
    if (a > b) {
      console.log(i + ". " + Math.round(Math.random() * (a - b) + b));
    } else {
      console.log(i + ". " + Math.round(Math.random() * (b - a) + a));
    }
  }
}

tenRandom(5, 10);

// TODO: 13. დავწეროთ ფუნქცია, რომელიც გამოიტანს 20 ფოტოდან შემთვევით
// რომელიმე 4 ფოტოს.
// TODO: 14. დავწეროთ ფუნქცია, რომელიც გამოიტანს კვირის რომელიმე
// შემთხვევით დღეს რაიმე ფოტოსთან ერთად.
// TODO: 15. დავწეროთ ფუნქცია, რომელიც გამოიტანს კვირის რომელიმე
// შემთხვევით დღეს ხუთი ფოტოდან რაიმე შემთხვევითად აღებულ
// ფოტოსთან ერთად.
// TODO: 16. დავწეროთ ფუნქცია, რომელიც გამოიტანს თვის რომელიმე შემთხვევით
// რიცხვს.
// TODO: 17. დავწეროთ ფუნქცია, რომელიც გამოიტანს თვის რომელიმე შემთხვევით
// რიცხვს ათი ფოტოდან რაიმე შემთხვევითად აღებულ ფოტოსთან
// ერთად.
// TODO: 18. დავწეროთ ფუნქცია, რომელიც გამოიტანს 3x4 ცხრილს, რომლის
// თითოეულ უჯრაში ჩასმული იქნება შემთხვევითად აღებული ფოტო 20
// ფოტოდან.
// TODO: 19. შექმენით ფორმა, რომელიც საშუალებას მოგვცემს ავირჩიოთ
// ცხრილისთვის სასურველი რაოდენობის სვეტები და სტრიქონები, ასევე

// ავირჩიოთ სასურველი რაოდენობის ფოტოები, ღილაკზე დაკლიკებისას
// გამოვიტანოთ არჩეული რაოდენობის სვეტებისა და სტრიქონების
// ცხრილი, რომელშიდაც იქნება ჩასმული არჩეული რაოდენობის
// ფოტოები.

// TODO: 10. დავწეროთ ფუნქცია, რომელიც დაბეჭდავს პარამეტრად გადაცემულ n
// შემთხვევით მთელ რიცხვს ასევე პარამეტრებად გადაცემული [a, b]
// მთელი შუალედიდან.

function task10(a, b) {
  if (a > b) {
    console.log(
      "n = " + Math.round(Math.random() * (a - b) + b) + ` Shualedi ${b} , ${a}`
    );
  } else {
    console.log(
      "n = " + Math.round(Math.random() * (b - a) + a) + ` shualedi ${a} , ${b}`
    );
  }
}

task10(10, 25);

// TODO: 11. დავწეროთ ფუნქცია, რომელიც გამოიტანს კვირის რომელიმე
// შემთხვევით დღეს.

function randomDay() {
  n = Math.round(Math.random() * (7 - 1) + 1);
  switch (n) {
    case 1:
      console.log("Orshabati");
      break;
    case 2:
      console.log("Samshabati");
      break;
    case 3:
      console.log("Otxshabati");
      break;
    case 4:
      console.log("Xutshabati");
      break;
    case 5:
      console.log("Paraskevi");
      break;
    case 6:
      console.log("Shabati");
      break;
    case 7:
      console.log("Kvira");
      break;
    default:
      console.log("Araswori parametri");
  }
}

randomDay();

// TODO: 12. დავწეროთ ფუნქცია, რომელიც გამოიტანს ათი ფოტოდან შემთხვევით რომელიმე ფოტოს.
